<?php

include_once('config/database.php');

if($_SERVER['REQUEST_METHOD'] != 'POST') {
    die('Unsupported request method');
}

$userId =  isset($_SESSION['user_id']) ? $_SESSION['user_id'] : false;
$conversationId =  isset($_POST['id']) ? $_POST['id'] : false;

if(!$userId || $conversationId) {
    die('Invalid Request');
}



if ($mysqli->connect_errno) {
    die("Could not connect to DB: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error);
}


$sql = 'UPDATE tbl_messages SET read_flag = 0 WHERE posting_user_id != ? AND conversation_id = ?';

$stmt = $mysqli->prepare($sql);
$stmt->bind_param('s', $userId);
$stmt->bind_param('s', $conversationId);

$stmt->execute();

echo json_encode(['success']);