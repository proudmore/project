<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

include_once('config/database.php');
include_once('config/general.php');
include_once('include/PHPMailer.php');
include_once('include/SMTP.php');
include_once('include/Exception.php');
include_once('include/functions.php');

// echo  generateRandomString();

$method = isset($_GET['method']) ? $_GET['method'] : false;

if (!$method) {
    die('method value not found');
}
// Script Author    : Karamjeet Singh
// Dated            : 3 Nov, 2018
// Purpose          : To save chat message
if ($method == 'chat') {
    $mysqli = new mysqli($database['host'], $database['user'], $database['pass'], $database['name']);
    if ($mysqli->connect_errno) {
        die("Could not connect to DB: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error);
    }


    $chatmessage = isset($_POST['chatMess']) ? $_POST['chatMess'] : false;
    $convID = isset($_POST['conv_ID']) ? $_POST['conv_ID'] : false;
    $refID = isset($_POST['ref_ID']) ? $_POST['ref_ID'] : false;
    $userID = isset($_SESSION['user_id']) ? $_SESSION['user_id'] : false;

    if (empty($chatmessage)) {
        echo json_encode(['status' => 'error', 'error' => 'All fields are required']);
        exit;
    }

    $sql = <<<SQL
INSERT INTO tbl_messages 
(conversation_id, reference_ad_id, posting_user_id, message_text, created_date, deleted) 
values
(?, ?, ?, ?, sysdate(), '0')
SQL;

    $stmt = $mysqli->prepare($sql);

    $stmt->bind_param('s', $convID);
    $stmt->bind_param('s', $refID);
    $stmt->bind_param('s', $userID);
    $stmt->bind_param('s', $chatmessage);

	$res = $stmt->execute();


	$response =<<<HTML
<div class="cell-content" style="display: block">
    <div class="ad_id">
        Message#3
    </div class="self">
        $chatmessage
    <div 
</div>
HTML;

    echo json_encode(['status' => true, 'message' => 'saved', 'html' => $response]);
    flush();
    ob_flush();
    $mysqli = null;
    exit;
}


// //UPLOAD FILES HANDLER
// if ($method == 'chat') {

//      print_r($_POST);
//      die('values are here');

// 	$mysqli = new mysqli($database['host'],  $database['user'], $database['pass'], $database['name']);
// 	if ($mysqli->connect_errno) {
// 		die("Could not connect to DB: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error);
// 	}

// 	$chatmessage = isset($_POST['chatMess']) ? $_POST['chatMess'] : false;	
// 	// $email = isset($_POST["email"]) ? $_POST['email'] : false;	
// 	// $city = isset($_POST["city"]) ? $_POST['city'] : false;
// 	// $description = isset($_POST["description"]) ? $_POST['description'] : false;
// 	// $premium = isset($_POST["premium"]) ? $_POST['premium'] : false;
// 	// $files = isset($_POST['files']) ? $_POST['files'] : false;
// 	// echo $chatmessage;
// 	// die;
// 	if (empty($chatmessage)){
// 		// echo 'PostVariables'. $_POST;
// 		echo json_encode(['status' => 'error', 'error' => 'All fields are required']);
// 		die;
// 	}

// 	/*$sqlQuery = "UPDATE tbl_freelancers SET name='$name', title='$title', country='$country',
// 		keywords='$keywords', portfolio='$portfolio', hourly_rate='$hourly_rate', taken='2' WHERE cell_number=$cell_number and taken=0";
// 	*/
// 	function randomstring($len)
// 	{
// 	$string = "";
// 	$chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
// 	for($i=0;$i<$len;$i++)
// 	$string.=substr($chars,rand(0,strlen($chars)),1);
// 	return $string;
// 	}

// 	// $activation_code = base64_encode(random_bytes(10)); // ~14 chars
// 	$activation_code = randomstring(15);
// 	$sqlQuery = "INSERT INTO tbl_messages (message_id,conversation_id,reference_ad_id,posting_user_id,message_text,created_date,deleted)
// 	values ('110','110','101','7',$chatmessage,sysdate(),'0')";
// 	$mysqli->query($sqlQuery);

// 	if (! empty($files)){
// 		$ad_id = $mysqli->insert_id;
// 		foreach ($files as $file){
// 			$query = "INSERT INTO tbl_files (url, ad_id) VALUES ('$file', '{$ad_id}')";
// 			$mysqli->query($query);
// 		}
// 	}

// 	//sending DONE to browser
// 	echo json_encode(['status'=>'ok']);
//     flush();
//     ob_flush();

// 	//generating confirm URL
// 	$url = $config['base_url'] . 'email_confirm.php?activation_code='.$activation_code.'&email=' . $email;
// 	//taking email template
// 	$template = file_get_contents('templates/confirm_guest_email.html');
// 	//setting values
// 	$template = str_replace('{{city}}', $city, $template);
// 	$template = str_replace('{{title}}', $title, $template);
// 	$template = str_replace('{{description}}', $description, $template);
// 	$template = str_replace('{{url}}', $url, $template);
// 	$subject = 'Confirm your Email Address before your Ad goes live';
// 	send_email($email, $subject, $template);

// 	//sending paypal link
// 	if ($premium){

// 		//building paypal url
// 		$url = 'https://www.paypal.com/cgi-bin/webscr?&cmd=_xclick&business='.$config['paypal_acc'].'&currency_code=USD&amount=5&item_name=Featured Ad (30 days)';

// 		//taking template
// 		$template = file_get_contents('templates/paypal_payment.html');
// 		//setting values
// 		$template = str_replace('{{city}}', $city, $template);
// 		$template = str_replace('{{title}}', $title, $template);
// 		$template = str_replace('{{description}}', $description, $template);
// 		$template = str_replace('{{url}}', $url, $template);
// 		$subject = 'Premium Ad - Please complete the Payment';    
// 		send_email($email, $subject, $template);
// 	}	
// }