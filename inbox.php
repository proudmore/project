<!DOCTYPE html>
<?php
include_once realpath(dirname(__FILE__)) . '/config/general.php';
include_once realpath(dirname(__FILE__)) . '/config/database.php';
include_once realpath(dirname(__FILE__)) . '/config/func_time2str.php';
include_once realpath(dirname(__FILE__)) . '/php/getinboxjson.php';
include_once realpath(dirname(__FILE__)) . '/include/functions.php';


$colors = array('#DAF7A6', '#95D9FF','#ECFF00',
                '#FDD7FF','#FFC7C7','#ABFF00','#FEC7FF',
                '#D5DBDB','#85B4FF','#FFFE00','#85DBFF');

?>
<html lang="en">
<head>
    <title>Document</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=3.0, user-scalable=yes">
    <base href="/" />
    <!-- base href="/adsbycity.com/" / -->
    <!-- base href="/adsbycity.com/" / -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />
    <link href="https://fonts.googleapis.com/css?family=Archivo:400,500|Arimo|Open+Sans+Condensed:300,700|Roboto:400|PT+Sans+Narrow" rel="stylesheet">
    <link rel="stylesheet" href="assets/animate/animate.css">
    <link rel="stylesheet" href="css/jquery.fileupload.css">
    <link rel="stylesheet" href="css/jquery.fileupload-ui.css">
    <link rel="stylesheet" href="//blueimp.github.io/Gallery/css/blueimp-gallery.min.css">
    <link rel="stylesheet" href="css/main.css?v=7">
    <link rel="stylesheet" href="css/popup.css">
    <link rel="icon" type="image/png" href="favicon.png" />
    <link rel="apple-touch-icon" href="apple-touch-favicon.png"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <style>
        .label-unread{
            display: inline;
            padding: 0.3em .5em .3em;
            font-size: 40%;
            font-weight: 500;
            line-height: 1;
            color: #fff;
            text-align: center;
            white-space: nowrap;
            vertical-align: top;
            border-radius: .25em;
            background-color: #1946df;
            margin-right: 8px;
            margin-left: 10px;
        }
        .msg-icon{
            color: #25a554 !important;
            margin-left: 10px;
        }
    </style>
</head>
<body class="page">
<div class="page-wrapper" id ="wrapper">
    <?php if(isset($_SESSION['user_name'])) { ?>
        <div class="user_login_container">
            <div class="logged_in_user">
                <label for="fav-checkbox"><? echo $_SESSION['user_name'];?></label>
                <a href="home">Home</a>
                <a href="logout">Logout</a>
            </div>
        </div>
    <? } else { ?>
        <div class="user_login_container">
            <div class="logged_in_user">
                <a href="register">New Users</a>
                <a href="login">Login</a>
            </div>
        </div>
    <? } ?>

    <div class="cell-container" id="field">

        <?php
        // echo $inboxjson;
        $array = json_decode($inboxjson);

        //count($array)
        $html='';//wait
        for($i=0; $i<count($array); $i++) {//wait one minute
        $data=$array[$i];
        ?>
        <div data-id="<?php echo $data->ref_ad_id ?>" data-convId="<?= $data->conv_id ?>" id="<?php echo $i+1;?>" onmouseover="hoverin(jQuery(this))" onmouseout="hoverout(jQuery(this))" ontouchstart="showPopup(jQuery(this))" class="cell <?php if($i<15) {echo 'visible initial-animation-state not-animated';} else {echo 'not-visible initial-animation-state not-animated';}?> bounceIn" data-num="<?php echo $i;?>" data-selected="green" data-hcolor="<?php echo $colors[$i%10];?>" data-bgcolor="<?php echo '#FFF';?>" title="std tooltip" data-title="std text" >
            <div class="cell__inner">
                <?php
                echo '<i class="fa fa-envelope msg-icon"></i>';
                echo '<span class="unread-count" id="unread-msg"><span class="label-unread">'.$data->unread.' New</span></span>';
                echo '<span class="city">'.$data->from_user_name.'</span>';
                $add = strlen($data->title) > 30 ? '..' : '';
                echo '<span class="title" data-title="Re: '.trim_string($data->title, 30).$add.'" data-title-full="Re: '.$data->title.'">Re: '.trim_string($data->title, 30).$add.'</span>'; //etc
                echo '<span class="email"></span>'; //etc
                //echo '<span class="heart fa fa-heart-o"></span>';
                // echo '<span class="description">'.trim_string($data->info->description, 50).'</span>'; //etc
                echo '<span class="postedon datefixed">'.$data->max_dt.'</span>'; //etc
                echo '<span class="arrow-container"><div class="arrow arrow-down"></div></span>'; //etc
                echo '</div>';
                echo '<div class="scrollcontainer">';
                $msgarray = $data->messages;
                // echo '<div name="formdiv"><!--messages Div starts here-->';
                for($j=0; $j<count($msgarray); $j++) {
                    $msgdata=$msgarray[$j];
                    echo '<div class="cell-content">';

                    echo '<div class="ad_id">Message#'.$msgdata->msg_id.'</div>';
                    echo '<div class="'. $msgdata->self.'" >' .$msgdata->msg_text .'</div>';;
                    echo '</div>';
                    echo '<div><span class="postedon datefixed">'.$msgdata->dt.'</span></div>';
                }
                // echo '<div name="formdiv"><!--Form Div starts here-->';
                echo '<div class="cell-content response">';
                echo '<div class="ad_id">Respond Text:</div>';
                echo '<form id="chatForm" class="field-value" action="sendchat.php?method=chat" method="POST" enctype="multipart/form-data">';

                echo '<div style="width:80%" class="'. $msgdata->self.'" ><input type="text" id="msgResponse" placeholder="Type here...." maxlength=200></div>';

                echo '<div style="width:20%; float:right;">';
                echo '<input type="hidden" name="convID" id="convID" value="'.$data->conv_id.'" >';
                echo '<input type="hidden" name="refID" id="refID" value="'.$data->ref_ad_id.'" >';
                echo '<button type="button" id="submitChat" class="active submitChat">Send</button>';
                echo '</div>';
                echo '</form>';
                echo '</div>';
                echo '</div>';
                echo '</div>'; //Scrollcontainer Div ends here
                }

                ?>

                <span class="go-up"><img src="images/up.png" alt=""></span>

</body>

<script defer src="assets/jquery/jquery-3.2.1.min.js"></script>
<!-- <script defer src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> -->
<script defer src="script/js.cookie.js"></script>
<script defer src="script/main2.js?v=3"></script>
</html>