<?php

// include_once realpath(dirname(__FILE__)) . '../config/func_time2str.php';
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

// include_once('../config/database.php');
//THis above call is must for calling this script from the browser directly

//echo 'City is ' . $_REQUEST['city'];
$wherecity = "";
$wheresearch = "";
if(isset($_REQUEST['city'])) {
	$input_city = $_REQUEST['city'];
	$wherecity = " and city = '". $input_city ."'";
}	
if(isset($_REQUEST['search'])) {
	$search_string = $_REQUEST['search'];
	$wheresearch = " and ((city like '%". $search_string ."%') OR (title like '%". $search_string ."%') OR (description like '%". $search_string ."%'))";
}	

$mysqli = new mysqli($database['host'],  $database['user'], $database['pass'], $database['name']);
if ($mysqli->connect_errno) {
    die("Could not connect to DB: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error);
}

$return_arr = array();
$subarray = array();
// echo $_SESSION['user_id'];
/*$querytext = "SELECT ad.id, ad.title, msg.message_id, msg.created_date, msg.message_text, msg.posting_user_id, usr.user_name, msg.created_date, DATE_FORMAT(msg.created_date, '%d-%b-%y') as createddate 
				FROM tbl_ads ad, tbl_messages msg, register_user usr 
				WHERE ad.id=msg.reference_ad_id and ad.user_id=:signed_in_user
				AND msg.posting_user_id = usr.register_user_id
				ORDER BY msg.created_date desc"; */
/*$querytext = "SELECT distinct ad.id, ad.title, msg.conversation_id, msg.posting_user_id, usr.user_name
				FROM tbl_ads ad, tbl_messages msg, register_user usr 
				WHERE ad.id=msg.reference_ad_id and ad.user_id=:signed_in_user
				AND msg.posting_user_id <> ad.user_id
				AND msg.posting_user_id = usr.register_user_id
				ORDER BY msg.created_date desc";*/
$querytext = "SELECT ad.id, ad.title, msg.conversation_id, msg.posting_user_id, usr.user_name, sum(read_flag) as unread, max(msg.created_date) as max_created_dt
				FROM tbl_ads ad, tbl_messages msg, register_user usr 
				WHERE ad.id=msg.reference_ad_id and ad.user_id=:signed_in_user
				AND msg.posting_user_id <> ad.user_id
				AND msg.posting_user_id = usr.register_user_id
				GROUP BY ad.id, ad.title, msg.conversation_id, msg.posting_user_id, usr.user_name                
				ORDER BY msg.created_date desc";


// echo $querytext;
$statement = $connect->prepare($querytext);
$statement->execute(
 array(
   'signed_in_user' => $_SESSION['user_id']
 )
);
$count = $statement->rowCount();
// echo 'Count of Messages: ';
// echo  $count;
if($count > 0)
{
  $result = $statement->fetchAll();
  $counter = 0;
  foreach($result as $row)
  { 					
// $fetch = $mysqli->query($querytext); 	
// while ($row = $fetch->fetch_array(MYSQLI_ASSOC)) {
    //$row_array['id'] = $row['ad_id'];
    $row_array = [];
    $row_array['counter'] = 0;
    $row_array['ref_ad_id'] = $row['id'];
	// $row_array['message_id'] = $row['message_id'];
	// $row_array['created_date'] = $row['created_date'];
	$row_array['title'] = $row['title'];
	$row_array['from_user_name'] = $row['user_name'];
	// $row_array['txt'] = $row['message_text'];
	$row_array['from_user'] = $row['posting_user_id'];
	$row_array['conv_id'] = $row['conversation_id'];	
	$row_array['unread'] = $row['unread'];	
	$row_array['max_dt'] = $row['max_created_dt'];	

		$subquerytext = "SELECT ad.id, msg.conversation_id, msg.posting_user_id, usr.user_name, msg.message_id, msg.message_text, msg.created_date
		FROM tbl_ads ad, tbl_messages msg, register_user usr 
		WHERE ad.id=msg.reference_ad_id and ad.user_id=:signed_in_user
		AND msg.conversation_id = :query_conversation_id
		AND msg.posting_user_id = usr.register_user_id
		ORDER BY msg.message_id desc";

		// echo $subquerytext;

		$msgstatement = $connect->prepare($subquerytext);
		$msgstatement->execute(
	 	array(
			'signed_in_user' => $_SESSION['user_id'],
			'query_conversation_id' => $row['conversation_id']
		 ));
		 $msgcount = $statement->rowCount();
		// echo 'Count of Messages: ';
		// echo  $count;
		if($msgcount > 0)
		{
		 $msgresult = $msgstatement->fetchAll();

		 $innercounter = 0;
		 $msgouterarray = array();
		 foreach($msgresult as $msgrow)
		 {
			$msgarray = [];
			// $msgarray['innercounter'] = 0;
			$msgarray['ref_ad_id'] = $msgrow['id'];
			$self = 'other'; //whether message is from the regd User
			if($msgrow['posting_user_id']==$_SESSION['user_id']) {
				$self = 'self';
			}
			$msgarray['self'] = $self;
			$msgarray['msg_id'] = $msgrow['message_id'];
			$msgarray['msg_text'] = $msgrow['message_text'];
			$msgarray['dt'] = $msgrow['created_date'];
			$msgarray['poster_id'] = $msgrow['posting_user_id'];
			$msgarray['poster_name'] = $msgrow['user_name'];
								/*array('' => 'dummy', 
								'msg_id' => 101,
								'msg_text' => 'Hello'						
								// 'created_date' => time2str($row['created_date']),
								);*/
			$msgouterarray[$innercounter] = $msgarray;
			$innercounter = $innercounter + 1;
			}
			$row_array['messages'] = $msgouterarray;
		}
	                            
	$subarray[$counter] = $row_array;
	//array_push($return_arr,$subarray);
    $counter = $counter + 1;
    

	}
}

$inboxjson= json_encode($subarray);

//echo '<pre>';
//print_r($subarray);
// echo $inboxjson;
?>