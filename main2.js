let start_time;
let options;
let rows;
let parent = jQuery('#field');
let last_low;
let animation_runing = false;
let json_data;
let int;
let run_animation;
var path_folder = './css/';
var searchJson=new Array();

function generateContent(arguments, length){
  let options = arguments || {};
  let row_count;
  let html ='';
  let index = parseInt(options.col_count) || 1;
  row_count = Math.ceil(length/index);

  /*html += '<div class="responsive_div_container" style="width:100%;">';*/
  for (let j = 0; j < row_count; j++) {
    html += generateRow(options, j, length);
  }
  /*html += "</div>";*/
  $('.responsive_div_container').css('width', $('#field').width())
  // console.log($('.responsive_div_container').css('width'));
  options = null;
  row_count = null;
  return(html);
}

function generateRow(arguments, num, length){
  let options = arguments || {};
  let total = length || 0;
  let index = parseInt(options.col_count) || 1;
  let col_count = (total > ((num+1)*index))? index : total - num*index;
  let row_html ='';
  let cell_width = calculateCellWidth(col_count);
  //let cell_width = calculateCellWidth2(arguments);
  //let cell_width = calcCellWidth(col_count);
  let row_class="row";
  let row_height;
  let row_number = num || 0;
  let json_id, data;
  let margin_bottom = options.row_space || 0;

  for (let i = 0; i <col_count; i++) {
    json_id = row_number*index+i;
    data = json_data[json_id];
    row_html += generateCell(options, i, cell_width, data, row_number+1);
  }

  options = null;
  col_count = null;
  cell_width = null;
  return (row_html);
}

let color_ind = 0;
let texture_ind = 0;
function generateCell(arguments, num, cell_width, data, row_number){
  let options = arguments || {};
  let json_cell = data || {};
  let height = options.height || 'auto';
  // let text = options.text || num;
  let text;
  let mydataid = json_cell.id;
  let cellfavorited;
  let cellviewed;
  let mytrimmedtitle = json_cell.info.title.substring(0,30);
  let myfulltitle = json_cell.info.title;
  let mytrimmeddesc = json_cell.info.description.substr(0,49);
  let mydesc = json_cell.info.description;
  let mycity = json_cell.info.city;
  let mycreateddate = json_cell.info.created_date;
  let margin_right = options.col_space || 0;
  let taken = json_cell.taken || 0;
  let popup_text = json_cell.txt || "";
  let border_color = json_cell.border || "#999";
  let hover_color = json_cell.bg; //json_cell.hover || "#fff";
  let animation_duration = parseInt(options.animation_duration)/1000 || .5;
  let cell_class = (run_animation) ? 'cell not-animated initial-animation-state' : 'cell';
  let cell_bg = '#fff'; //json_cell.bg || '#fff';
  let cell_selected_color = json_cell.sel || '#fff';
  let cross_icon = json_cell.icon  || false;
  let comment = json_cell.tooltip || '';
  let font_selected
  if(jQuery('#fontfamily').length){
    font_selected = jQuery('#fontfamily').val();
  }else{
    font_selected = 'sans-serif';
  }
  let font = json_cell.font || font_selected;
  let margin_bottom = options.row_space || 0;
  
  text = (parseInt(text) == 0) ? row_number: parseInt(text)+1;

  // text = (cross_icon) ? '<img src="'+cross_icon+'" alt="x">' : text;
  //text = generateCellText(json_cell);
  textures = ['circles.png','dimension.png','abc.png','flower-trail.png','black-lozenge.png','maze-black.png',
			'diagonal-striped-brick.png','batthern.png','dark-circles.png','graphy-dark.png','always-grey.png',
			'arabesque.png','arches.png','3px-tile.png','bright-squares.png','checkered-pattern.png',
			'robots.png','project-paper.png','random-grey-variations.png'];  
  colors = ['#DAF7A6','#ECFF00','#95D9FF','#fca4f9','#ABFF00','#FEC7FF','#fff200', //'#ff736e', //'#FFC7C7',
      '#00e0b3','#85B4FF','#f5d400','#00d4f0','#87F717'];
  cell_bg='#FFF'; 
  hover_color = colors[color_ind];
  //cell_bg = colors[color_ind];
  
  color_ind++;
  color_ind = (color_ind > 11)? 0 : color_ind;
  texture_ind++;
  texture_ind = (texture_ind > 18)? 0 : texture_ind;
  
  var bg_image = path_folder+textures[texture_ind];
//############################## Chung(start) ##############################
  var cellId = json_cell.counter;
  cell_html = '<div id="' + cellId + '"'+
    'taken="' + taken + '"' +
    'onmouseover ="hoverin(jQuery(this))"'+
    'onmouseout ="hoverout(jQuery(this))"'+
    'ondblclick ="showPopup(jQuery(this))"'+
    'ontouchstart ="showPopup(jQuery(this))"'+
    'class="'+cell_class+'" '+
    'data-num="'+num+'"'+
    'data-selected = "'+cell_selected_color+'"'+
    'data-hcolor="'+hover_color+'"'+
    'data-bgcolor="'+cell_bg+'"'+
    'title ="'+comment+'"'+
    'data-title="'+popup_text+'"'+
    'style="border-color:'+border_color+'; animation-duration:'+animation_duration+'s; margin-right:'+margin_right+'px; background-color:'+cell_bg+';'+
		"background-image: url('"+bg_image+"');"+
		'font-family:'+font+';margin-bottom:'+margin_bottom+'px;"'+
    '"><div class="cell__inner" >'+  // because 
    text+ '</div>'+
    '<div class="cell-content">Here is some cell content</div>'+
    '</div>';
	
  var favs = Cookies.getJSON('favs');
    if (favs !== undefined ){        
            if (favs.indexOf(parseInt(mydataid)) > -1){
				cellfavorited = ' selected ';
            }                    
    }
	
  var views = Cookies.getJSON('views');
    if (views !== undefined ){        
            if (views.indexOf(parseInt(mydataid)) > -1){
				cellviewed = 'viewed';
            }                    
    }

	cell_html = '<div data-id="'+mydataid+'" id="'+cellId+'" taken="2" paid="1" onmouseover="hoverin(jQuery(this))" onmouseout="hoverout(jQuery(this))" ontouchstart="showPopup(jQuery(this))" class="cell not-visible '+cellviewed+' initial-animation-state not-animated" data-num="'+num+'" data-selected="'+cell_selected_color+'" data-hcolor="'+hover_color+'" data-bgcolor="'+cell_bg+'" title="std tooltip" data-title="std text"><div class="cell__inner">' +
	'<span class="fav '+cellfavorited+'"></span>' +
	'<span class="city">'+mycity +'</span>' +
	'<span class="title" data-title="'+mytrimmedtitle+'" data-title-full="'+myfulltitle+'">'+ mytrimmedtitle +'</span>' +
	'<span class="email"></span>' +
	'<span class="description">'+mytrimmeddesc+'</span>' +
	'<span class="postedon datefixed">'+mycreateddate+'</span>' +
	'<span class="arrow-container"><div class="arrow arrow-down"></div></span>' +
	'</div>' +
	'<div class="cell-content"><div class="ad_id">'+mydataid+'</div><div class="descr">'+mydesc+'</div>' +
	'</div>' +
	'</div>';
//############################## Chung(end) ##############################
  options = null;
  height = null;
  text = null;
  popup_text = null;
  //console.log(cell_html);
  return(cell_html);
  
}

//############################## Chung(end) ##############################

function calculateCellWidth(col_count){
  let options = arguments || {};
  let width = jQuery('#field').width(); 
  //- 20 + (parseInt(options.col_space)*( parseInt(options.col_count)-1));
  let result = Math.floor(width/col_count);
 
  //let result =width/col_count;  
  result -= parseInt(options.col_space)||0;
  
  options['cell_width'] = result;
  return(result);
}
function calcCellWidth(arguments){
  let options = arguments || {};
  let parent_width = jQuery('#field').width();
  let cell_width = Math.floor((parent_width)/((parseInt(options.col_count))||parseInt(options)));  // COL_COUNT
  // cell_width -= 35;
 if (typeof(options)=='object')
	cell_width -= parseInt(options.col_space);
  else
	cell_width -= parseInt(options);
  return (cell_width);

}

function changeCellSize(width, cells){

  cells_width = Math.max(width, 240);
  cells_height = .5*cells_width;
  /*cells.height(cells_height).width(cells_width).css({'max-width':cells_width +'px','min-width':cells_width +'px', 'max-height': cells_height+'px', 'line-height': cells_height+'px'});*/
}

function changeCellsVisibility(cells, arguments){
    options = arguments || {}
    cells.each(function(ind,el){
      cell = jQuery(this);
      visibile = checkCellsVisibility(cell);
    })
}

function checkCellsVisibility(cell){
  let set_top = window.scrollY;
  let parent = jQuery('#field');
  let start_y = parent['0'].offsetTop;
  let window_height = jQuery(window).height();
  let row_offset_y, result;
    row_offset_y = cell.offset().top;
    result = (row_offset_y > set_top + window_height )? false : true;
    if(result){
      cell.addClass('visible').removeClass('not-visible');
    }else{
      cell.addClass('not-visible').removeClass('visible');
    }
    return(result);
}

function animateCells(cells, arguments, index){
  let options = arguments || {};
  let animation = options.animation || 'fadeIn';
  let timeout;

  animation_runing = true;
  cell = jQuery('#field').find('.visible.not-animated').eq(0);

  runAnimation(cell, options,index);
}

function runAnimation(cell, options){
  let animation_interval = options.animation_interval || 10;
  let animation = options.animation || 'fadeIn';
  if(cell.length){
    int = setTimeout(function(){
       cell.addClass('animated').addClass(animation).removeClass('initial-animation-state').removeClass('not-animated');
       new_cell = cell.siblings('.visible.not-animated').eq(0);
       runAnimation(new_cell, options);
     },animation_interval);
  }else{
    animation_runing = false;
    return(false);
  }
}

//############################## Chung(start) ##############################
/*function getJsonData(){
  let xhr = new XMLHttpRequest();
  xhr.open('GET', 'json.txt', false);
  //xhr.open('GET', 'php/getjson2.php', false);
  xhr.send();
  if (xhr.status != 200) {
     return false;
  } else {
    data = JSON.parse(xhr.responseText);
  }

  return(data);
}*/

function getJsonSearchData(searchstr){
  console.log('getJsonSearchData - start');
  let xhr = new XMLHttpRequest();
  //xhr.open('GET', 'json.txt', false);
  xhr.open('GET', 'php/getjsonsearch.php?search='+searchstr, false);
  xhr.send();
  if (xhr.status != 200) {
     return false;
  } else {
    data = JSON.parse(xhr.responseText);
  }

  console.log('getJsonSearchData - end');
  return(data);
}

//############################## Chung(end) ##############################

function calculateJsonLength(json){
//console.log('incoming json: '+JSON.stringify(json));
  var result = JSON.stringify(json).match(/"counter"/g).length;
  return(result);
}

jQuery('.stop').click(function(e) {
  if(int){
    clearTimeout(int);
  }
})

/*jQuery(document).ready(function(){
if(!jQuery('.go').length){
  if(int){
    clearTimeout(int);
  }
  let html;
  let message;

  options = {
    // row_count : jQuery('#rows').val(),
    col_count : 4,
    animation_interval : 150,
    animation_duration : 400,
    animation : 'bounceIn',
    row_space :  5,
    col_space :  5,
  }

  run_animation = (jQuery('#no-animation').prop('checked')) ? false : true;
  start_time = new Date();
  parent.html('');
  json_data = getJsonData();
  let total_count = calculateJsonLength(json_data);
  html = generateContent(options, total_count);

  parent.append(html);
  cells = jQuery('.cell-container').find('.cell');
  let width =  calcCellWidth(options);
  //let width = calculateCellWidth2(options);

  changeCellSize(width, cells);
  changeCellsVisibility(cells,options);
  animateCells(cells, options);
  html = null;
  $(window).trigger('resize');
}
})*/

jQuery(document).ready(function(){
  options = {
    // row_count : jQuery('#rows').val(),
    col_count : 1,
    animation_interval : 150,
    animation_duration : 1000,
    animation : 'bounceIn', //'bounceIn', 'bounceInRight', 'fadeIn'
    row_space :  0,
    col_space :  0,
  }
  //json_data = getJsonData();
  //let total_count = calculateJsonLength(json_data);

  cells = jQuery('.cell-container').find('.cell');
  animateCells(cells, options);
});



/*jQuery('.go').click(function(e) {
  e.preventDefault();
  if(int){
    clearTimeout(int);
  }
  let html;
  let message;

  options = {
    // row_count : jQuery('#rows').val(),
    col_count : '4',
    animation_interval : jQuery('#interval').val(),
    animation_duration : jQuery('#duration').val(),
    animation : jQuery('#animation-type').val(),
    row_space :  jQuery('#row_space').val(),
    col_space :  jQuery('#col_space').val(),
  }

  run_animation = (jQuery('#no-animation').prop('checked')) ? false : true;
  start_time = new Date();
  parent.html('');
  json_data = getJsonData();
  let total_count = calculateJsonLength(json_data);
  html = generateContent(options, total_count);

  parent.html(html);
  cells = jQuery('.cell-container').find('.cell');
  let width =  calcCellWidth(options);

  changeCellSize(width, cells);
  changeCellsVisibility(cells,options);
  animateCells(cells, options);
  
  html = null;
});*/

jQuery(window).on('scroll', function(){
  //alert('onscroll');
  cells = jQuery('#field').find('.cell');
  changeCellsVisibility(cells,options);
  jQuery('.go-up').addClass('shown');
  //if(!animation_runing){
    cells = jQuery('.cell-container').find('.not-visible');
    animateCells(cells, options);
  //}
})

jQuery('.go-up').click(function(){
  jQuery('html,body').animate({scrollTop: 0}, 300);
  setTimeout(function(){
    jQuery('.go-up').removeClass('shown');
  }, 350);
})
jQuery(window).resize(function(){
  cells = jQuery('.cell-container').find('.cell');
  let width =  calcCellWidth(options);

  changeCellSize(width, cells);
})

jQuery('.close-popup').on('click', function(e) {
  jQuery(this).closest('.popup-wrapper').removeClass('show');
});

function hoverin(obj){
  let color =(obj.hasClass('selected')) ?obj.data('selected') : obj.data('hcolor');
  obj.css({'background-color': color});
}

function hoverout(obj){
  let color =(obj.hasClass('selected')) ?obj.data('selected') : obj.data('bgcolor');
  obj.css({'background-color': color});
}

//############################## Chung(start) ##############################
$(function() {
	$(document).on("keyup", function(e) {
		if(e.key=="Escape") $('.popup-wrapper').removeClass('show');
	});

	$(".popup #btn-add-skill").click(function() {
		var skill = $(".popup #new_skill").val();
		addSkill(skill);
		$(".popup #new_skill").val("");
	});

/*	$(document).on("click", ".popup .skill-wrapper .delete-skill", function() {
		$(this).parent().remove();
		//$(this).parent().detach();
		//null;
	});
*/	
	$(".btn_container #myAddButton").click(function() {
	var cell;
	cell = $("#99999"); 
	//cell = $(".cell").first(); //$("#2");
	//alert('99999 does not exist yet');
	showPopup(cell);
  })
  $(".popup input[type=text], .popup textarea").keydown(function(){
    $('.popup .error').hide();
    $('.popup input, .popup textarea').removeClass('input-error');
  })
	$(".popup #btn-preview").click(function() {
		var popup = $(".popup");
		var cellId = popup.attr("target-cell-id");
		var title = popup.find("#title").val();
		var email = popup.find("#email").val();
		var city = popup.find("#city").val();
		var description = popup.find("#description").val();
    var paidflag = popup.find("#cbPaid").prop("checked")?1:0;

    if ($.trim(title) == ''){
      $('.popup .error').html('Title is required').show();
      popup.find("#title").addClass('input-error');
      return;
    }
    if ($('#ademail').val() == ''){      
      $('.popup .error').html('Email is required').show();
      popup.find("#ademail").addClass('input-error');
      return;
    }
    if (! validateEmail(email)){
      $('.popup .error').html('Email ' + email + ' is not valid').show();
      popup.find("#ademail").addClass('input-error');
      return;
    } 
    if ($.trim(city) == ''){
      $('.popup .error').html('City is required').show();
      popup.find("#city").addClass('input-error');
      return;
    }
    if (! /^[a-zA-Z \']+$/.test(city)){
      $('.popup .error').html('City can have only letters, space and apostrophe').show();
      popup.find("#city").addClass('input-error');
      return;
    }
    if ($.trim(description) == ''){
      $('.popup .error').html('Description is required').show();
      popup.find("#description").addClass('input-error');
      return;
    }    

		var emptyCellhtml = getEmptyCellRahul();
		var mycontainer = $(".cell-container");
		$("#99999").remove();
		var findtag;
		if (paidflag==0) {   //FREE LISTING
			findtag = $( ".cell[paid='1']" ).last();
			findtag.after(emptyCellhtml);
		}
		else {     //PAID LISTING
			findtag = $( ".cell[paid='1']" ).first();
			findtag.before(emptyCellhtml);
		}
		
		var cell = $("#99999");
		cell.attr("paid", paidflag);
		cell.find(".title").text(title);
		cell.find(".email").text(email);
		cell.find(".city").text(city);
		cell.find(".description").text(description.substring(0,49));
		cell.find(".temporary").text('Preview');
		
		$('.popup-wrapper').removeClass('show');
		//$('.btn_container #myAddButton').hide(); //('show');
	});
  // Code below for Submitting the Chat Message using sendchat.php
  $(".submitChat").click(function() {
      
      // var chatText = $('#msgResponse').val();
      // var convID = $('#convID').val();
      // var refID = $('#refID').val();
      // var This = $(this);
      
      var This = $(this);
      var chatText = This.parent().prev().find('#msgResponse').val();
      var convID = This.prev().prev().val();
      var refID = This.prev().val();
      
      
      //  console.log(convID);
      //  console.log(refID);return false;//update pls
      $.post("sendchat.php?method=chat", {chatMess:chatText,conv_ID:convID,ref_ID:refID}, function(response){
                
          if(response.status == true){
            // console.log('1:');
            // console.log(This.parent());
            // console.log('2:');
            // console.log(This.parent().parent());
            // console.log('3:');
            // console.log(This.parent().parent().prev());
            // console.log('4:');
            // console.log(This.parent().parent().parent());
            This.parent().parent().parent().before(response.html);
              //This.parent().parent().parent().append(response.html);
//              This.parent().parent().parent().parent().'

          }
        }, 'json');
      });
      
  
//    var chat_text = $('#msgResponse').val();
    // alert('Validate Form here..with message: ' + chatmessage);
//    var data = {
      // cell_number: cellId,
//      chatmessage: chat_text,
//      files: $('input[name="urls[]"]').map(function(){return $(this).val();}).get()
      // email: email,
      // description: description,
      // city: city,
      // premium: paidflag
//    };
    
    //Nov 2 2018
    //Karamjeet Singh
//    var formData = [];
//    formData['chatmessage'] = chat_text;
//    var data = formData.serialize();
    
//    $.post("sendchat.php?method=chat", formData, function(response){}, 'json');
//
//    console.log(response);
//    // $(this).unbind('click');
//  
//  });

	$(".popup #btn-submit").click(function() {
    $('.popup .error').hide();
    var popup = $(".popup");
    var cellId = popup.attr("target-cell-id");
    var title = popup.find("#title").val();
    var email = popup.find("#ademail").val();
    var city = popup.find("#city").val();
    var description = popup.find("#description").val();
    var paidflag = popup.find("#cbPaid").prop("checked")?1:0;

    if ($.trim(title) == ''){
      $('.popup .error').html('Title is required').show();
      popup.find("#title").addClass('input-error');
      return;
    }

    if ($.trim(email) == ''){      
      $('.popup .error').html('Email is required').show();
      popup.find("#ademail").addClass('input-error');
      return;
    }
    if (! validateEmail(email)){
      $('.popup .error').html('Email is not valid').show();
      popup.find("#ademail").addClass('input-error');
      return;
    } 
    if ($.trim(city) == ''){
      $('.popup .error').html('City is required').show();
      popup.find("#city").addClass('input-error');
      return;
    }
    if (! /^[a-zA-Z \']+$/.test(city)){
      $('.popup .error').html('City can have only letters, space and apostrophe').show();
      popup.find("#city").addClass('input-error');
      return;
    }
    if ($.trim(description) == ''){
      $('.popup .error').html('Description is required').show();
      popup.find("#description").addClass('input-error');
      return;
    }  

    var data = {
      cell_number: cellId,
      title: title,
      email: email,
      description: description,
      city: city,
      premium: paidflag,
      files: $('input[name="urls[]"]').map(function(){return $(this).val();}).get()
    };
    $.post("ajax.php?method=add_ad", data, function(response){}, 'json');

    $(this).unbind('click');

    var emptyCellhtml = getEmptyCellRahul();
    var mycontainer = $(".cell-container");
    var emptyCellhtml = getEmptyCellRahul();
    $("#99999").remove();
    var findtag;
    if (paidflag==0) {   //FREE LISTING
      findtag = $( ".cell[paid='1']" ).last();
      findtag.after(emptyCellhtml);
    }
    else {     //PAID LISTING
      findtag = $( ".cell[paid='1']" ).first();
      findtag.before(emptyCellhtml);
    }
    /*if ($("#99999").length == 0) {
      //mycontainer.append(emptyCellhtml);	
      mycontainer.prepend(emptyCellhtml);	
    }*/
        
    var cell = $("#99999");
        cell.attr("paid", paidflag);
    cell.find(".title").text(title);
        cell.find(".city").text(city);
        cell.find(".email").text(email);
        cell.find(".description").text(description.substring(0,49));
    cell.find(".temporary").text('Pending Approval');
    cell.off("ondblclick");
        $('.popup-wrapper').removeClass('show');
    $('.btn_container #myAddButton').hide(); //('show');
  });
});

/*function getEmptyCell() {
  var template = "";
  template += "<li><span><span class='title'/> <span class='smaller city'/></span></li>";
  template += "<li><span/></li>";
  template += "<li><span><span class='description'/></span></li>";
  template += "<li><span><span class='link' target='blank_'><i class='fa fa-link'/><span class='portfolio-url'/></span></span></li>";
  template += "<li><span><span class='smaller'>Hourly rate:$<span class='hourly-rate'/>/hr</span></span></li>";
  template += "<li><span><div class='tag-cont skills'/></span></li>";
  return $(template);
}*/

function getEmptyCellRahul() {
    var cellId = 99999; //json_cell.counter;
  cell_html = '<div id="' + cellId + '"'+
    'taken="' + '0' + '"' +
    'onmouseover ="hoverin(jQuery(this))"'+
    'onmouseout ="hoverout(jQuery(this))"'+
    'ondblclick ="showPopup(jQuery(this))"'+
    'ontouchstart ="showPopup(jQuery(this))"'+
    'class="'+'cell visible animated bounceIn'+'" '+
    'data-num="'+cellId+'"'+
    'data-selected = "'+'#CCC'+'"'+
    'data-hcolor="'+'#DAF7A6'+'"'+
    'data-bgcolor="'+'#FFF'+'"'+
    'title ="'+'STD TEXT'+'"'+
    'data-title="'+'STD TEXT'+'"'+
    'style="border-color:'+'rgb(204, 204, 204);'+'; animation-duration:'+'0.4'+'s; width: 100%; margin-right:'+'1'+'px; background-color:'+'#FFF'+';'+
		"font-family: sans-serif; background-image: url('"+'abc.png'+"');"+
    '"><div class="cell__inner" >'+  // because 
	  "<span class='fav'></span><span class='smaller city'/></span><span class='title'>My Sample Ad Title</span><span class='email'></span><span class='smaller description'>My Sample Ad Description</span><span class='smallest temporary fixed'></span>"+
    '</div></div>';

	var template = "";
  template += "</span><span><span class='title'/> <span class='smaller city'/></span></li>";
  template += "<li><span/></li>";
  template += "<li><span><span class='description'/></span></li>";
  template += "<li><span><span class='link' target='blank_'><i class='fa fa-link'/><span class='portfolio-url'/></span></span></li>";
  template += "<li><span><span class='smaller'>Hourly rate:$<span class='hourly-rate'/>/hr</span></span></li>";
  template += "<li><span><div class='tag-cont skills'/></span></li>";
  return $(cell_html);
}

var skillTemplate = "<div class='skill-wrapper'>";
skillTemplate += "<span class='material-icons delete-skill'>delete</span>";
skillTemplate += "<span class='skill'></span>";
skillTemplate += "</div>";

function addSkill(skill) {
  if(skill.length == 0) return;
  var skillSet = $(".popup .skillset");
  var exist = 0;
  skillSet.find(".skill").each(function() {
	if($(this).text().toLowerCase() === skill.toLowerCase()) {
		exist = 1;
		return;
	}
  });
  if(exist == 1) return;
  var skillWrapper = $(skillTemplate);
  skillWrapper.find(".skill").text(skill);
  skillSet.append(skillWrapper);
}

function showPopup(cell){
  //let text = cell.find('ul').clone();

  //var taken = cell.attr("taken");
  var taken;
  var cellId = cell.attr("id");
  var title = '';
  var email = '';
  var city = '';
  var description = '';
	if(cellId == '99999'){  //
		//alert('Preview Cell already Exists');  //(inserted)
		title = cell.find(".title").text();
		email = cell.find(".email").text();
		city = cell.find(".city").text();
		description = cell.find(".description").text();
		taken = 0;
	}
	else{  //
		//alert('Preview Cell does not exist');
		//title = 'My Sample Ad Title';
		//email = 'abc@xyz.com';
		//city = 'My City';
		//description = 'My Sample Ad Description';
		taken = 0; //cell.attr("taken");  //Don't show any Popup YET.
	}
  var popup = $(".popup");
  popup.attr("taken", taken);
  popup.attr("target-cell-id", cellId);
  popup.find("#title").val(title);
  popup.find("#email").val(email);
  popup.find("#city").val(city);
  popup.find("#description").val(description);
  
  
  // var skillSet = popup.find(".skillset");
  // popup.find(".skillset").empty();
  // skills.forEach(function(skill) {
  // jQuery('.popup__text').html(text);
    // addSkill(skill);
  // });
	//if(cellId == '99999') {
		jQuery('.popup-wrapper').addClass('show');
		jQuery('#field').find('.cell').addClass('grey');
	//}
  // cell.addClass('selected');
  // let color =(cell.hasClass('selected')) ?cell.data('selected') : cell.data('bgcolor');
  // cell.css({'background-color': color});
}
//############################## Chung(end) ##############################

function showPopup2(cell){
  if(jQuery(window).width() < 1000){
    let text = cell.find('ul').clone();
    jQuery('.popup__text').html(text);
    jQuery('.popup-wrapper').addClass('show');
    jQuery('#field').find('.cell').addClass('grey');
    // cell.addClass('selected');
    // let color =(cell.hasClass('selected')) ?cell.data('selected') : cell.data('bgcolor');
    // cell.css({'background-color': color});
  }
}
//############################## RAHUL (start)###################################
function showPopupRahul(cell){

  var taken = cell.attr("taken");
  var cellId = cell.attr("id");
  var title = cell.find(".title").text();
  var email = cell.find(".email").text();
  var city = cell.find(".city").text();
  var title = cell.find(".title").text();

  
  var popup = $(".popup");
  popup.attr("taken", taken);
  popup.attr("target-cell-id", cellId);
  popup.find("#title").val(title);
  popup.find("#email").val(email);
  popup.find("#city").val(city);
  popup.find("#title").val(title);
  popup.find("#new_skill").val("");
  
  jQuery('.popup-wrapper').addClass('show');
  jQuery('#field').find('.cell').addClass('grey');
  // cell.addClass('selected');
  // let color =(cell.hasClass('selected')) ?cell.data('selected') : cell.data('bgcolor');
  // cell.css({'background-color': color});
}
//############################## RAHUL (end)#####################################

function changeColor(cell){
    cell.toggleClass('selected');
    let color =(cell.hasClass('selected')) ?cell.data('selected') : cell.data('bgcolor');
     cell.css({'background-color': color});
}

jQuery('#fontfamily').on('change',function(){
  jQuery('#field').find('.cell').css({'font-family': jQuery(this).val()});
})

jQuery('#search__field').keyup(function(e){
	console.log('key pressed ' + e.key + ' with value:' + this.value);
	if(e.key!="Enter") return;
	var searchString=this.value;
	//json_data = getJsonData();
	json_search_data = getJsonSearchData(searchString);
  searchJson=[];
  console.log('searchString:'+searchString);
	//if(!$.isEmptyObject(searchString)){
	/*if(searchString){//same operate
		/*$.each(json_data,function(key,data){
			//if(data.info.skills){
				if ( (!$.isEmptyObject(data.info.title)  &&  data.info.title.toLowerCase().indexOf(searchString.toLowerCase()) !=-1) ||
					(!$.isEmptyObject(data.info.description) && data.info.description.toLowerCase().indexOf(searchString.toLowerCase()) !=-1)  ) {  
					searchJson.push(data);
				}
			//}
		});
		$.each(json_search_data,function(key,data){ 
			searchJson.push(data);
		})
	} */
	/*else {*/
		searchJson=json_search_data;
	/*}*/
	//searchJson=JSON.parse(searchArray);
	//console.log("my data: "+JSON.stringify(searchJson));
	let total_count = calculateJsonLength(searchJson);
	
	html = generateSearchContent(options, total_count);
	parent.html('');
	//alert(html);
	parent.append(html);
	cells = jQuery('.cell-container').find('.cell');
	let width =  calcCellWidth(options);
	changeCellSize(width, cells);
	changeCellsVisibility(cells,options);
	animateCells(cells, options);
	
	
	
	$(window).trigger('resize');
	html = null;
});

function generateSearchContent(arguments, length){
  let options = arguments || {};
  let row_count;
  let html ='';
  let index = parseInt(options.col_count) || 1;
  row_count = Math.ceil(length/index);
  
  //html += '<div class="responsive_div_container" style="width:100%;">';
  for (let j = 0; j < row_count; j++) {
    html += generateSearchRow(options, j, length);
  }
  //html += "</div>";
  // console.log(options);
  //$('.responsive_div_container').css('width', $('#field').width())
  // for (let j = 0; j < row_count; j++) {
    // html += generateSearchRow(options, j, length);
  // }
  options = null;
  row_count = null;
  return(html);
}
function generateSearchRow(arguments, num, length){
  let options = arguments || {};
  let total = length || 0;
  let index = parseInt(options.col_count) || 1;
  let col_count = (total > ((num+1)*index))? index : total - num*index;
  let row_html ='';
  //let cell_width = calculateCellWidth(col_count);
  let cell_width = calcCellWidth(col_count);
  let row_class="row";
  let row_height;
  let row_number = num || 0;
  let json_id, data;
  let margin_bottom = options.row_space || 0;

  for (let i = 0; i <col_count; i++) {
    json_id = row_number*index+i;
    data = searchJson[json_id];
    row_html += generateCell(options, i, cell_width, data, row_number+1);
	//row_html += FilledRowRahul(options, i, cell_width, data, row_number+1);
  }

  options = null;
  col_count = null;
  cell_width = null;
  return (row_html);
}

function validateEmail(email) {
  var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}

function checkFavButton(){
  var favs = Cookies.getJSON('favs');
  if (favs == undefined || favs.length == 0){
    $('.favorite_filter').hide();
  } else {
    $('.favorite_filter').show();
  }
}

function checkViewedButton(){
  var views = Cookies.getJSON('views');
  if (views == undefined || views.length == 0){
    $('.views_filter label').hide();
  } else {
    $('.views_filter label').show();
  }
}

function checkAccordionButton(){
  if ($('.cell .arrow.active').length > 0){
    //$('.accordion_filter button').show();
	$('.accordion_filter label').removeClass('inactive').addClass('active');
  } else {
    //$('.accordion_filter button').hide();
	$('.accordion_filter label').removeClass('active').addClass('inactive');
  }
}

var views_state = 0;  //0 - all, 1 - viewed, 2 - unviewed
var collapse_state = 0;  //0 - all, 1 - viewed, 2 - unviewed

$(document).on('ready', function() {
    //set favorite icons
    var favs = Cookies.getJSON('favs');
    if (favs !== undefined ){        
        $('.cell').each(function(){
            var id = $(this).data('id');
            if (favs.indexOf(id) > -1){
                $(this).find('.fav').addClass('selected');
            }            
        });        
    }
})

$(document).on('click', '.fav', function(e) {
        e.stopPropagation();
        var favs = Cookies.getJSON('favs');
        if (favs == undefined) favs = [];
        var id = $(this).closest('.cell').data('id');
        if ($(this).hasClass('selected')){
            var index = favs.indexOf(id);
            if (index > -1) {
                favs.splice(index, 1);
            }                    
            Cookies.set('favs', JSON.stringify(favs), {expires: 365});
            $(this).removeClass('selected');
            if ($('input[name="fav_filter"]').is(':checked')){
              $(this).closest('.cell').hide();
              if ($('.fav.selected').length == 0){
                $('.no-items').show();
                setTimeout(function(){
                  $('.no-items').hide();
                  $('input[name="fav_filter"]').prop('checked', false).change();
                }, 3000);
              }
            }            
        } else {
            favs.push(id);
            Cookies.set('favs', JSON.stringify(favs), {expires: 365});
            $(this).addClass('selected');
        }

        //hide button if no favorites
        checkFavButton();
    });

$(document).on('click', '.cell', function(event) {
// $('.cell').click(function(){     
    // alert(event.target.id);
    var target = 'msgResponse';
    var target2 = 'submitChat';
    // console.log(event.target);
    // if(event.target.id == target) { alert('True');}
    if(event.target.id != target && event.target.id != target2) {
        $(this).addClass('viewed');           
        $(this).find('.cell-content').slideToggle();
        $('.cell').removeClass('bgc');
        $(this).toggleClass('bgc');
        if ($(this).find('.arrow').hasClass('active')) {
        $(this).removeClass('bgc');
        }
        $(this).find('.arrow').toggleClass('active');
        checkAccordionButton();        
        //$(this).find('.title, .description, .city, .datefixed').css('font-weight', 'normal');
        var id = $(this).data('id');

        $.ajax({
            url: 'readUpdater.php',
            type: 'POST',
            data: {
              id: this.dataset.convid
            }
        });


        var views = Cookies.getJSON('views');
        if (views == undefined) views = [];
        if (views.indexOf(id) < 0){
            views.push(id);
            Cookies.set('views', JSON.stringify(views), {expires: 365});
        }
        if ($(this).find('.arrow').hasClass('active')){            
            $(this).find('.title').html($(this).find('.title').data('title-full'));
            $(this).find('.description').hide();
        } else {            
            $(this).find('.title').html($(this).find('.title').data('title'));
            $(this).find('.description').show();
        }    
        checkViewedButton();    

    }
});

$(document).ready(function(){  
    //initial set of fav button
    checkFavButton();

    //set favorite icons
    var favs = Cookies.getJSON('favs');
    if (favs !== undefined ){        
        $('.cell').each(function(){
            var id = $(this).data('id');
            if (favs.indexOf(id) > -1){
                $(this).find('.fav').addClass('selected');
            }            
        });        
    }

    var views = Cookies.getJSON('views');
    if (views !== undefined){
        checkViewedButton();
        $('.cell').each(function(){
            var id = $(this).data('id');
            if (views.indexOf(id) > -1){
                $(this).addClass('viewed');
                //$(this).find('.title, .description, .city, .datefixed').css('font-weight', 'normal');
            }
        });
    }
    
    //collapse all 
    $('.accordion_filter').click(function(){
       $('.cell-content').slideUp();
       $('.cell__inner .arrow').removeClass('active');
	   $('.cell').removeClass('bgc');
       //$('.accordion_filter button').hide();
      //if (collapse_state == 0){
        //collapse_state = 1;
        //$('.cell').not('.viewed').hide();
        //$('.views_filter label').html('Viewed').removeClass('all').addClass('viewed');
        $('.accordion_filter label').removeClass('active').addClass('inactive');
        return;
      //}
    });

    $('.views_filter').click(function(){
      $('.cell').show();
      if (views_state == 0){
        views_state = 1;
        $('.cell').not('.viewed').hide();
        $('.views_filter label').html('Viewed').removeClass('all').addClass('viewed');
        return;
      }
      if (views_state == 1){
        views_state = 2;
        $('.cell.viewed').hide();
        $('.views_filter label').html('Unviewed').removeClass('viewed').addClass('unviewed');
        return;
      }
      views_state = 0;
      $('.views_filter label').html('ALL').removeClass('unviewed').addClass('all');
    });

    //show only favs or all
    $('input[name="fav_filter"]').change(function(){
        if ($(this).is(':checked')){
            $('.fav').not('.selected').closest('.cell').hide();
        } else {
            $('.fav').closest('.cell').show();
        }
    })

    //allow only letters, space and apostrof
    $("#city").keypress(function(e) {
        var key = e.keyCode;
        if ( (key > 64 && key < 91) || (key > 96 && key < 123) || key == 39 || key == 32) {  //
          //allow to go  
        } else {
          e.preventDefault();
        }
    });
});